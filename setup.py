from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(include=['pizzadoos_manuel']),
    version='0.1.0',
    description='Pizzadoos applicatie',
    author='Manuel Rodriguez Ruiz',
    license='BSD-3',
)